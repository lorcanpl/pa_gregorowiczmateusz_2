using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using zad6_templateMethod_csharp.Controllers;
using zad6_templateMethod_csharp.Enums;
using zad6_templateMethod_csharp.Interfaces;

namespace zad6_templateMethod_csharp_tests
{
    [TestClass]
    public class EndToEndTests
    {
        private const string NameOfObject = "UJ's first car";
        private const uint NumberOfObjectsToConstruct = 1;
        private Mock<IObjectsConstructor> ObjectsConstructor { get; set; }
        private Mock<IConstructionRecipeCreator> ConstructionRecipeCreator { get; set; }
        private Mock<ILogger> Logger { get; set; }
        private Mock<IProductionLineMover> ProductionLineMover { get; set; }
        private Mock<IConstructionRecipe> ConstructionRecipe { get; set; }
        private ControllerTemplateMethod Controller { get; set; }

        // dodanie testinitialize bez konstruktora
        [TestInitialize]
        public void Init()
        {
            ObjectsConstructor = new Mock<IObjectsConstructor>(MockBehavior.Strict);
            ConstructionRecipeCreator = new Mock<IConstructionRecipeCreator>(MockBehavior.Strict);
            Logger = new Mock<ILogger>(MockBehavior.Strict);
            ProductionLineMover = new Mock<IProductionLineMover>(MockBehavior.Strict);
            ConstructionRecipe = new Mock<IConstructionRecipe>(MockBehavior.Strict);
            Controller = new ControllerWithDependencyInjection(ObjectsConstructor.Object, ConstructionRecipeCreator.Object, Logger.Object, ProductionLineMover.Object);
        }

        [TestMethod]
        public void TestConstructionOfSingleObject()
        {
            const bool wasSuccessfull = true;
            ConstructionRecipe.Setup(foo => foo.NameOfObject).Returns(NameOfObject);
            ConstructionRecipeCreator.Setup(foo => foo.ConstructionRecipe).Returns(ConstructionRecipe.Object);
            ConstructionRecipeCreator.Setup(foo => foo.NumberOfElementsToProduce).Returns(NumberOfObjectsToConstruct);
            ProductionLineMover.Setup(foo => foo.MoveProductionLine(MovingDirection.Forward)).Returns(wasSuccessfull);
            ObjectsConstructor.Setup(foo => foo.ConstructObjectFromRecipe(ConstructionRecipe.Object)).Returns(wasSuccessfull);
            Logger.Setup(foo => foo.Log(LoggingType.Info, It.IsAny<string>()));

            Controller.Execute();

            ConstructionRecipe.Verify(foo => foo.NameOfObject);
            ConstructionRecipeCreator.Verify(foo => foo.ConstructionRecipe, Times.Once);
            ConstructionRecipeCreator.Verify(foo => foo.ConstructionRecipe, Times.Once);
            ObjectsConstructor.Verify(foo => foo.ConstructObjectFromRecipe(ConstructionRecipe.Object), Times.Once);
            Logger.Verify(foo => foo.Log(LoggingType.Info, It.IsAny<string>()), Times.Once);
            ProductionLineMover.Verify(foo => foo.MoveProductionLine(MovingDirection.Forward), Times.Exactly(2));
        }

        [TestMethod]
        public void TestConstructionFailure_MovingProductionLineFailed()
        {
            ConstructionRecipe.Setup(foo => foo.NameOfObject).Returns(NameOfObject);
            ConstructionRecipeCreator.Setup(foo => foo.ConstructionRecipe).Returns(ConstructionRecipe.Object);
            ConstructionRecipeCreator.Setup(foo => foo.NumberOfElementsToProduce).Returns(NumberOfObjectsToConstruct);
            ProductionLineMover.Setup(foo => foo.MoveProductionLine(MovingDirection.Forward)).Throws(new Exception("Can't move ProductionLine!"));

            Logger.Setup(foo => foo.Log(LoggingType.Error, It.IsAny<string>()));

            Controller.Execute();

            ConstructionRecipe.Verify(foo => foo.NameOfObject);
            ConstructionRecipeCreator.Verify(foo => foo.ConstructionRecipe, Times.Once);
            ConstructionRecipeCreator.Verify(foo => foo.ConstructionRecipe, Times.Once);
            ObjectsConstructor.Verify(foo => foo.ConstructObjectFromRecipe(ConstructionRecipe.Object), Times.Never);
            Logger.Verify(foo => foo.Log(LoggingType.Info, It.IsAny<string>()), Times.Never);
            ProductionLineMover.Verify(foo => foo.MoveProductionLine(MovingDirection.Forward), Times.Once);
        }

        [TestMethod]
        public void TestConstructionFailure_ObjectConstructionFailed()
        {
            var wasSuccessfull = true;
            ConstructionRecipe.Setup(foo => foo.NameOfObject).Returns(NameOfObject);
            ConstructionRecipeCreator.Setup(foo => foo.ConstructionRecipe).Returns(ConstructionRecipe.Object);
            ConstructionRecipeCreator.Setup(foo => foo.NumberOfElementsToProduce).Returns(NumberOfObjectsToConstruct);
            ProductionLineMover.Setup(foo => foo.MoveProductionLine(MovingDirection.Forward)).Returns(wasSuccessfull);
            ObjectsConstructor.Setup(foo => foo.ConstructObjectFromRecipe(ConstructionRecipe.Object)).Returns(wasSuccessfull = false);
            Logger.Setup(foo => foo.Log(LoggingType.Warning, It.IsAny<string>()));
            ProductionLineMover.Setup(foo => foo.MoveProductionLine(MovingDirection.ToScran)).Returns(wasSuccessfull = true);

            Controller.Execute();

            ConstructionRecipe.Verify(foo => foo.NameOfObject);
            ConstructionRecipeCreator.Verify(foo => foo.ConstructionRecipe, Times.Once);
            ConstructionRecipeCreator.Verify(foo => foo.ConstructionRecipe, Times.Once);
            ObjectsConstructor.Verify(foo => foo.ConstructObjectFromRecipe(ConstructionRecipe.Object), Times.Once);
            Logger.Verify(foo => foo.Log(LoggingType.Warning, It.IsAny<string>()), Times.Once);
            ProductionLineMover.Verify(foo => foo.MoveProductionLine(MovingDirection.Forward), Times.Once);
            ProductionLineMover.Verify(foo => foo.MoveProductionLine(MovingDirection.ToScran), Times.Once);
        }

        [TestMethod]
        public void TestConstructionFailure_ObjectConstructionFailedThenMovingToScanAlsoFailed()
        {
            var wasSuccessfull = true;
            ConstructionRecipe.Setup(foo => foo.NameOfObject).Returns(NameOfObject);
            ConstructionRecipeCreator.Setup(foo => foo.ConstructionRecipe).Returns(ConstructionRecipe.Object);
            ConstructionRecipeCreator.Setup(foo => foo.NumberOfElementsToProduce).Returns(NumberOfObjectsToConstruct);
            ProductionLineMover.Setup(foo => foo.MoveProductionLine(MovingDirection.Forward)).Returns(wasSuccessfull);
            ObjectsConstructor.Setup(foo => foo.ConstructObjectFromRecipe(ConstructionRecipe.Object)).Returns(wasSuccessfull = false);
            Logger.Setup(foo => foo.Log(LoggingType.Warning, It.IsAny<string>()));
            ProductionLineMover.Setup(foo => foo.MoveProductionLine(MovingDirection.ToScran)).Throws(new Exception("Can't move not constructed car to scan, scan is full!"));
            Logger.Setup(foo => foo.Log(LoggingType.Error, It.IsAny<string>()));

            Controller.Execute();

            ConstructionRecipe.Verify(foo => foo.NameOfObject);
            ConstructionRecipeCreator.Verify(foo => foo.ConstructionRecipe, Times.Once);
            ConstructionRecipeCreator.Verify(foo => foo.ConstructionRecipe, Times.Once);
            ObjectsConstructor.Verify(foo => foo.ConstructObjectFromRecipe(ConstructionRecipe.Object), Times.Once);
            Logger.Verify(foo => foo.Log(LoggingType.Warning, It.IsAny<string>()), Times.Once);
            Logger.Verify(foo => foo.Log(LoggingType.Error, It.IsAny<string>()), Times.Once);
            ProductionLineMover.Verify(foo => foo.MoveProductionLine(MovingDirection.Forward), Times.Once);
            ProductionLineMover.Verify(foo => foo.MoveProductionLine(MovingDirection.ToScran), Times.Once);
        }
    }
}