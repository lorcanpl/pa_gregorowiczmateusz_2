namespace zad6_templateMethod_csharp.Enums
{
    public enum LoggingType
    {
        Error,
        Warning,
        Info
    }
}