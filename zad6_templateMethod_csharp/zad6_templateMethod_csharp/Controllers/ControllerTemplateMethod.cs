namespace zad6_templateMethod_csharp.Controllers
{
    public abstract class ControllerTemplateMethod
    {
        public virtual void Execute()
        {
        }
    }
}