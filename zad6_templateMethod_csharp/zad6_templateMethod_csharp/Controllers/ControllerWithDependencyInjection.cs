﻿using System;
using zad6_templateMethod_csharp.Enums;
using zad6_templateMethod_csharp.Interfaces;

namespace zad6_templateMethod_csharp.Controllers
{
    public class ControllerWithDependencyInjection : ControllerTemplateMethod
    {
        private readonly IConstructionRecipeCreator _constructionRecipeCreator;
        private readonly ILogger _logger;
        private readonly IObjectsConstructor _objectsConstructor;
        private readonly IProductionLineMover _productionLineMover;

        public ControllerWithDependencyInjection(IObjectsConstructor objectsConstructor,
            IConstructionRecipeCreator constructionRecipeCreator, ILogger logger, IProductionLineMover productionLineMover)
        {
            _objectsConstructor = objectsConstructor;
            _constructionRecipeCreator = constructionRecipeCreator;
            _logger = logger;
            _productionLineMover = productionLineMover;
        }

        public override void Execute()
        {
            var numberToCreate = _constructionRecipeCreator.NumberOfElementsToProduce;
            var constructionRecipe = _constructionRecipeCreator.ConstructionRecipe;
            for (var i = 0; i < numberToCreate; i++)
            {
                try
                {
                    _productionLineMover.MoveProductionLine(MovingDirection.Forward);
                    _productionLineMover.MoveProductionLine(!CreateObject(constructionRecipe) ? MovingDirection.ToScran : MovingDirection.Forward);
                }
                catch (Exception e)
                {
                    var message = string.Format("Error while creating {0} object\nMessage: {1}", constructionRecipe.NameOfObject, e.Message);
                    _logger.Log(LoggingType.Error, message);
                }
            }
        }

        private bool CreateObject(IConstructionRecipe recipe)
        {
            var createdSuccessfull = _objectsConstructor.ConstructObjectFromRecipe(recipe);
            if (createdSuccessfull)
                _logger.Log(LoggingType.Info, string.Format("Created {0} object", recipe.NameOfObject));
            else
                _logger.Log(LoggingType.Warning, string.Format("Creation object '{0}' failed", recipe.NameOfObject));

            return createdSuccessfull;
        }
    }
}