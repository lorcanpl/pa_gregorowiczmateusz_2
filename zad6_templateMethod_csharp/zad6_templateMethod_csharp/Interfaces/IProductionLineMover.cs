using zad6_templateMethod_csharp.Enums;

namespace zad6_templateMethod_csharp.Interfaces
{
    public interface IProductionLineMover
    {
        bool MoveProductionLine(MovingDirection direction);
    }
}