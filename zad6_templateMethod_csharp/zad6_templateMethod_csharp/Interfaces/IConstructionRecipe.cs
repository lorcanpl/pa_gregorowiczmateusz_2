﻿namespace zad6_templateMethod_csharp.Interfaces
{
    public interface IConstructionRecipe
    {
        string NameOfObject { get; }
    }
}