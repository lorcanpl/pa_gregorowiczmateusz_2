﻿using zad6_templateMethod_csharp.Enums;

namespace zad6_templateMethod_csharp.Interfaces
{
    public interface ILogger
    {
        void Log(LoggingType loggingType, string p);
    }
}