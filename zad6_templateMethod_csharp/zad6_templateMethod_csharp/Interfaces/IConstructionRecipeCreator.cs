﻿namespace zad6_templateMethod_csharp.Interfaces
{
    public interface IConstructionRecipeCreator
    {
        IConstructionRecipe ConstructionRecipe { get; set; }

        uint NumberOfElementsToProduce { get; set; }
    }
}