namespace zad6_templateMethod_csharp.Interfaces
{
    public interface IObjectsConstructor
    {
        bool ConstructObjectFromRecipe(IConstructionRecipe constructionRecipe);
    }
}