using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Zad2.TreeModel;

namespace Zad2.Enumerators
{
    internal class BreadthFirstSearchTreeEnumerator<T> : IEnumerator<T>
    {
        private readonly Tree<T> _tree;
        private readonly List<Tree<T>> _visited = new List<Tree<T>>();
        private readonly Queue<Tree<T>> _queue = new Queue<Tree<T>>();

        public BreadthFirstSearchTreeEnumerator(Tree<T> tree)
        {
            _tree = tree;
            Reset();
        }

        public void Dispose()
        {

        }

        public bool MoveNext()
        {
            if (!_queue.Any())
                return false;
            var next = _queue.Dequeue();
            foreach (var neighbor in next.Children)
            {
                if (!_visited.Contains(neighbor))
                {
                    _queue.Enqueue(neighbor);
                    _visited.Add(neighbor);
                }
            }
            Current = next.Value;
            return true;
        }

        public void Reset()
        {
            _queue.Clear();
            _visited.Clear();
            _queue.Enqueue(_tree);
            _visited.Add(_tree);
            Current = default(T);
        }

        public T Current { get; private set; }

        object IEnumerator.Current
        {
            get { return Current; }
        }
    }
}