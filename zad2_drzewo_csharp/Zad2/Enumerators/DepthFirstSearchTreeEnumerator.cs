using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Zad2.TreeModel;

namespace Zad2.Enumerators
{
    internal class DepthFirstSearchTreeEnumerator<T> : IEnumerator<T>
    {
        private readonly List<Tree<T>> _list = new List<Tree<T>>();
        private readonly Stack<Tree<T>> _stack = new Stack<Tree<T>>();
        private readonly Tree<T> _tree;
        private int _currentIndex;

        public DepthFirstSearchTreeEnumerator(Tree<T> tree)
        {
            _tree = tree;
            Reset();
        }

        public void Dispose()
        {
        }

        public bool MoveNext()
        {
            while (_stack.Any())
            {
                var n = _stack.Pop();
                _list.Add(n);
                foreach (var child in n.Children.ToArray().Reverse())
                {
                    _stack.Push(child);
                }
            }

            if (_currentIndex < _list.Count - 1)
            {
                _currentIndex++;
                Current = _list[_currentIndex].Value;
                return true;
            }
            return false;
        }

        public void Reset()
        {
            _list.Clear();
            _stack.Push(_tree);
            Current = default(T);
            _currentIndex = -1;
        }
        
        public T Current { get; private set; }

        object IEnumerator.Current
        {
            get { return Current; }
        }
    }
}