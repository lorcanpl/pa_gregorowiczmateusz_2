﻿namespace Zad2.TreeModel
{
    public enum EnumeratorOrder
    {
        BreadthFirstSearch = 1,
        DepthFirstSearch = 2
    }
}