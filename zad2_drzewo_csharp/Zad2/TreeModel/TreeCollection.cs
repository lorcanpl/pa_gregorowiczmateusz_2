﻿using System.Collections;
using System.Collections.Generic;

namespace Zad2.TreeModel
{
    public class TreeCollection<T> : ICollection<T>
    {
        private readonly List<T> _items = new List<T>();

        public IEnumerator<T> GetEnumerator()
        {
            return _items.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public void Add(T item)
        {
            _items.Add(item);
        }

        public void Clear()
        {
            _items.Clear();
        }

        public bool Contains(T item)
        {
            return _items.Contains(item);
        }

        public void CopyTo(T[] array, int arrayIndex)
        {
            for (var i = arrayIndex; i < array.Length; i++)
            {
                array[i] = _items[i];
            }
        }

        public bool Remove(T item)
        {
            return _items.Remove(item);
        }

        public int Count
        {
            get { return _items.Count; }
        }

        public bool IsReadOnly { get; private set; }

        public void AddRange(IEnumerable<T> children)
        {
            _items.AddRange(children);
        }
    }
}