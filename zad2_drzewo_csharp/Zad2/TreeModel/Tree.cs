﻿using System;
using System.Collections;
using System.Collections.Generic;
using Zad2.Enumerators;

namespace Zad2.TreeModel
{
    public class Tree<T> : IEnumerable<T>
    {
        private EnumeratorOrder _order;
        private Tree<T> _parent;

        public Tree(T value, EnumeratorOrder order)
        {
            Value = value;
            Order = order;
            Children = new TreeCollection<Tree<T>>();
        }

        public Tree(T value, EnumeratorOrder order, IEnumerable<Tree<T>> children)
        {
            Value = value;
            Order = order;
            Children.AddRange(children);
        }

        public T Value { get; private set; }

        public EnumeratorOrder Order
        {
            get
            {
                if (_parent == null)
                    return _order;

                var current = _parent;
                while (current._parent != null)
                    current = current._parent;
                return current.Order;
            }
            set
            {
                if (Enum.IsDefined(typeof (EnumeratorOrder), value))
                    _order = value;
                else
                    throw new ArgumentOutOfRangeException();
            }
        }

        public TreeCollection<Tree<T>> Children { get; private set; }

        public IEnumerator<T> GetEnumerator()
        {
            switch (Order)
            {
                case EnumeratorOrder.BreadthFirstSearch:
                    return new BreadthFirstSearchTreeEnumerator<T>(this);
                case EnumeratorOrder.DepthFirstSearch:
                    return new DepthFirstSearchTreeEnumerator<T>(this);
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public void Add(T child)
        {
            Children.Add(new Tree<T>(child, Order) {_parent = this});
        }

        public void Add(Tree<T> subtree)
        {
            subtree.Order = Order;
            subtree._parent = this;
            Children.Add(subtree);
        }
    }
}