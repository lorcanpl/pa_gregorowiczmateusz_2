package app;

import java.util.ArrayList;
import java.util.Iterator;

public class Composition<T> implements Executable<T>, Iterable<Executable<T>> {

    private ArrayList<Executable<T>> functions = new ArrayList<>();

    public Composition() {
        functions = new ArrayList<>();
    }

    public Composition(Executable<T> function) {
        functions.add(function);
    }

    public Composition(Executable<T> function1, Executable<T> function2) {
        functions.add(function1);
        functions.add(function2);
    }

    public Composition(ArrayList<Executable<T>> functions) {
        this.functions.addAll(functions);
    }

    public T execute(T parameter) {
        T value = parameter;
        for(Executable<T>  function : functions){
            value = function.execute(value);
        }
        return value;
    }

    public void add(Executable<T> function) {
        functions.add(function);
    }

    @Override
    public Iterator<Executable<T>> iterator() {
        return functions.iterator();
    }
}
