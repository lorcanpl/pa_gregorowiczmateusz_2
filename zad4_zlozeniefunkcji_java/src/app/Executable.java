package app;

public interface Executable<T> {

    T execute(T parameter);
}
