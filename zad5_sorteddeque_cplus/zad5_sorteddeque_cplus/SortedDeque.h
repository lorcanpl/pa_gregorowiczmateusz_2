#include <iostream>

using namespace std;

template <typename T>
struct SortedDequeItem
{
private:
	int number_occurences = 1;

public:
	T value;

	friend bool operator==(const SortedDequeItem& lhs, const SortedDequeItem& rhs)
	{
		return lhs.value == rhs.value;
	}

	friend bool operator<(const SortedDequeItem& lhs, const SortedDequeItem& rhs)
	{
		return lhs.value < rhs.value;
	}

	friend bool operator>(const SortedDequeItem& lhs, const SortedDequeItem& rhs)
	{
		return lhs.value > rhs.value;
	}

	friend bool operator<=(const SortedDequeItem& lhs, const SortedDequeItem& rhs)
	{
		return lhs.value <= rhs.value;
	}

	friend bool operator>=(const SortedDequeItem& lhs, const SortedDequeItem& rhs)
	{
		return lhs.value >= rhs.value;
	}

	void increase_occurences()
	{
		number_occurences++;
	}
};

template <typename T, int BUCKET_SIZE = INT_MAX>
class SortedDeque
{
private:
	unsigned used_capacity = 0;
	vector<SortedDequeItem<T>> items;

public:

	unsigned capacity()
	{
		return number_of_buckets() * BUCKET_SIZE;
	}

	bool empty()
	{
		return items.empty();
	}

	unsigned size_total() const
	{
		return used_capacity;
	}

	unsigned size_unique()
	{
		return items.size();
	}

	void reserve(const unsigned capacity_to_reserve)
	{
		unsigned bucket_count = static_cast<int>(ceil(static_cast<double>(capacity_to_reserve) / static_cast<double>(BUCKET_SIZE)));
		items.reserve(bucket_count * BUCKET_SIZE);
	}

	unsigned number_of_buckets()
	{
		double bucket_number = ceil(static_cast<double>(items.capacity()) / static_cast<double>(BUCKET_SIZE));
		return static_cast<int>(bucket_number);
	}

	void insert(T value)
	{
		auto foundItem = find_if(items.begin(), items.end(), [&value](const SortedDequeItem<T>& item)
		{
			return item.value == value;
		});

		if (foundItem != items.end())
		{
			auto index = std::distance(items.begin(), foundItem);
			SortedDequeItem<T>& updated_item = items[index];
			updated_item.increase_occurences();
		}
		else
		{
			int position = 0;
			for (unsigned i = 0; i < size_unique(); i++)
				if (at(i) < value) position = i + 1;

			SortedDequeItem<T> newItem;
			newItem.value = value;
			items.insert(items.begin() + position, newItem);
		}
		used_capacity++;
	}

	T at(unsigned i)
	{
		return items[i].value;
	}

	T back()
	{
		return items[items.size() - 1].value;
	}

	struct iterator : public std::iterator < bidirectional_iterator_tag, T >
	{ 
		iterator(vector<SortedDequeItem<T>> collection)
		{
			this->collection = collection;
		}

		const T& operator*() const
		{
			return collection.at(val).value;
		}

		bool operator!=(const iterator& it) const
		{
			return val != it.getCurrentIndex();
		}

		iterator& operator++()
		{
			++val;
			return *this;
		}

		void setEnd()
		{
			val = collection.size();
		}

		int getCurrentIndex() const
		{
			return val;
		}

	private:
		int val = 0;
		vector<SortedDequeItem<T>> collection;
	};

	iterator begin()
	{
		return iterator(items);
	}

	iterator end()
	{
		iterator i = iterator(items);
		i.setEnd();
		return i;
	}
};
