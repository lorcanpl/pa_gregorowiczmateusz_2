using System.Collections;
using System.Collections.Generic;

namespace Zad3.App
{
    public class TripleList<T> : IEnumerable<T>
    {
        internal bool HasValue;
        public TripleList<T> MiddleElement { get; private set; }
        public TripleList<T> PreviousElement { get; private set; }
        public TripleList<T> NextElement { get; private set; }
        public T Value { get; private set; }

        public IEnumerator<T> GetEnumerator()
        {
            return new TripleListEnumerator<T>(this);
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public int Count()
        {
            var counter = 0;
            var enumerator = GetEnumerator();
            while (enumerator.MoveNext())
                counter++;

            return counter;
        }

        public void Add(T value)
        {
            var current = this;
            while (current != null)
            {
                if (!current.HasValue)
                {
                    current.Value = value;
                    current.HasValue = true;
                    return;
                }
                
                if (TryAddMiddleElement(value, current)) 
                    return;
                if (TryAddNextElement(value, current)) 
                    return;
                
                current = current.NextElement;
            }
        }

        private static bool TryAddNextElement(T value, TripleList<T> current)
        {
            if (current.NextElement != null)
                return false;
            
            var nextElement = new TripleList<T>
            {
                Value = value,
                PreviousElement = current,
                HasValue = true
            };
            current.NextElement = nextElement;
            return true;
        }

        private static bool TryAddMiddleElement(T value, TripleList<T> current)
        {
            if (current.MiddleElement != null) 
                return false;
            
            var middleElement = new TripleList<T>
            {
                MiddleElement = current,
                HasValue = true,
                Value = value
            };
            current.MiddleElement = middleElement;
            return true;
        }

        public void Add(TripleList<T> value)
        {
            var enumerator = value.GetEnumerator();
            while (enumerator.MoveNext())
            {
                Add(enumerator.Current);
            }
        }
    }
}