using System.Collections;
using System.Collections.Generic;

namespace Zad3.App
{
    public class TripleListEnumerator<T> : IEnumerator<T>
    {
        private readonly TripleList<T> _mainTripleList;
        private TripleList<T> _currentElement;
        private TripleList<T> _previousElement;

        public TripleListEnumerator(TripleList<T> mainTripleList)
        {
            _mainTripleList = mainTripleList;
            Reset();
        }

        public void Dispose()
        {
        }

        public bool MoveNext()
        {
            if (_currentElement == null)
            {
                _currentElement = _mainTripleList;
                if (!_currentElement.HasValue)
                    return false;

                Current = _currentElement.Value;
                return true;
            }

            if (TryGetMiddleElement())
                return true;

            if (TryGetNextElement())
                return true;

            return false;
        }

        public void Reset()
        {
            _currentElement = null;
            _previousElement = null;
            Current = default(T);
        }

        public T Current { get; private set; }

        object IEnumerator.Current
        {
            get { return Current; }
        }

        private bool TryGetNextElement()
        {
            if (_currentElement.MiddleElement == null || _currentElement.MiddleElement.NextElement == null)
                return false;
            
            _currentElement = _currentElement.MiddleElement.NextElement;
            _previousElement = _currentElement.PreviousElement;
            Current = _currentElement.Value;
            return true;
        }

        private bool TryGetMiddleElement()
        {
            if (_currentElement.MiddleElement == null || _currentElement.MiddleElement == _previousElement)
                return false;
            
            _previousElement = _currentElement;
            _currentElement = _currentElement.MiddleElement;
            Current = _currentElement.Value;
            return true;
        }
    }
}