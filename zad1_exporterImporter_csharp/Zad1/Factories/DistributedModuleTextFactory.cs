﻿using Zad1.DataModel;
using Zad1.Export;
using Zad1.Import;

namespace Zad1.Factories
{
    public class DistributedModuleTextFactory : DistributedModuleFactory
    {
        public DistributedModuleTextFactory(string text): base(text)
        {
            
        }

        public override Data CreateData()
        {
            return new TextData(Text);
        }

        public override Exporter CreateExporter()
        {
            return new TextExporter(Text);
        }

        public override Importer CreateImporter()
        {
            return new TextImporter();
        }
    }
}