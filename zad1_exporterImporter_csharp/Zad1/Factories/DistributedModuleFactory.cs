﻿using Zad1.DataModel;
using Zad1.Export;
using Zad1.Import;

namespace Zad1.Factories
{
    public class DistributedModuleFactory
    {
        protected readonly string Text;

        protected DistributedModuleFactory(string text)
        {
            Text = text;
        }

        virtual public Data CreateData()
        {
            return new Data() {Text = Text};
        }

        virtual public Exporter CreateExporter()
        {
            return new Exporter() {ExportData = CreateData()};
        }

        virtual public Importer CreateImporter()
        {
            return new Importer();
        }
    }
}