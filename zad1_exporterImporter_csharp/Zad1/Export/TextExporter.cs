﻿using Zad1.DataModel;

namespace Zad1.Export
{
    public class TextExporter : Exporter
    {
        public TextExporter(string textToBeExported)
        {
            ExportData = new TextData(textToBeExported);
        }
    }
}