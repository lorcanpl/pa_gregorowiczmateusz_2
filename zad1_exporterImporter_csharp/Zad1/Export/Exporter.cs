﻿using Zad1.DataModel;

namespace Zad1.Export
{
    public class Exporter
    {
        public Exporter()
        {
            ExportData = new Data();
        }

        public Data ExportData { get; set; }
    }
}