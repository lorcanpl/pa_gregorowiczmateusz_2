﻿using Zad1.DataModel;

namespace Zad1.Import
{
    public class Importer
    {
        protected Data DataToSendToImporter;

        public void ImportData(Data dataToSendToImporter)
        {
            DataToSendToImporter = dataToSendToImporter;
        }
    }
}