﻿namespace Zad1.Import
{
    public class TextImporter : Importer 
    {
        public string ImportedText
        {
            get { return DataToSendToImporter.Text; }
        }
    }
}