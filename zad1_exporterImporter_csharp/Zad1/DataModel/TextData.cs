﻿namespace Zad1.DataModel
{
    public class TextData : Data
    {
        public TextData(string textToBeImported)
        {
            Text = textToBeImported;
        }
    }
}