﻿namespace Zad1.DataModel
{
    public class Data
    {
        private string _text;

        public string Text
        {
            get
            {
                var text = _text;
                _text = string.Empty;
                return text;
            }
            set { _text = value; }
        }
    }
}