﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Zad1.DataModel;
using Zad1.Export;
using Zad1.Factories;
using Zad1.Import;

namespace Zad1.Tests
{
    [TestClass]
    public class BasicTests
    {
        [TestMethod]
        public void TestExporter()
        {
            // Arrange
            var textToBeExported = "Ala ma kota";
            Exporter exporter = new TextExporter(textToBeExported);
            var exportedData = exporter.ExportData;
            var exportedText = (exportedData as TextData).Text;
            
            // Assert
            Assert.AreEqual(textToBeExported, exportedText);
            
            // Arrange
            exportedData = exporter.ExportData;
            exportedText = (exportedData as TextData).Text;
            textToBeExported = string.Empty;
            
            // Assert
            Assert.AreEqual(textToBeExported, exportedText);
        }

        [TestMethod]
        public void TestImporter()
        {
            // Arrange
            var textToBeImported = "Ala zgubila dolara";
            Data dataToSendToImporter = new TextData(textToBeImported);
            Importer importer = new TextImporter();
            
            // Act
            importer.ImportData(dataToSendToImporter);
            var dataSavedInImporter = (importer as TextImporter).ImportedText;
            
            // Assert
            Assert.AreEqual(textToBeImported, dataSavedInImporter);
        }

        [TestMethod]
        public void TestFactory()
        {
            // Arrange
            const string textToForFactory = "Ali kot zjadl dolara";
            DistributedModuleFactory factory = new DistributedModuleTextFactory(textToForFactory);
            // Act
            var dataFromFactory = factory.CreateData();
            var textFromModule = (dataFromFactory as TextData).Text;
            // Assert
            Assert.AreEqual(textToForFactory, textFromModule);
            
            // Act
            var exporter = factory.CreateExporter();
            textFromModule = ((exporter as TextExporter).ExportData as TextData).Text;
            // Assert
            Assert.AreEqual(textToForFactory, textFromModule);
            
            // Act
            var importer = factory.CreateImporter();
            // Assert
            Assert.IsTrue(importer is TextImporter);
        }
    }
}